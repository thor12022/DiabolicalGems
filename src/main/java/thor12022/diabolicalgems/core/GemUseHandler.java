package thor12022.diabolicalgems.core;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ArrayUtils;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.OreDictionary;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.config.Config;
import thor12022.diabolicalgems.config.Configurable;
import thor12022.diabolicalgems.util.MapSet;

@Configurable(sectionName=Configurable.SECTION_GENERAL, syncNotification="syncConfig")
class GemUseHandler
{   
   private static class ItemMapSet extends MapSet<Item, Integer>
   {
      private final Map<Item, Boolean> isWild = new HashMap<>();
      
      @Override
      public int put(Item item, Integer meta)
      {
         isWild.put(item, meta == OreDictionary.WILDCARD_VALUE);
         return super.put(item, meta);
      }

      @Override
      public boolean containsValue(Item item, Integer meta)
      {
         if(isWild.containsKey(item))
         {
            return isWild.get(item) ? true : super.containsValue(item, meta);
         }
         return false;
      }
      
      @Override
      public void clear()
      {
         super.clear();
         isWild.clear();
      }

      public void populate(String[] itemNames, String debugName)
      {
         populate(this, itemNames, debugName);
      }
      
      static public void populate(ItemMapSet mapSet, String[] itemNames, String debugName)
      {
         for(String rawItemName : itemNames)
         {
            String[] tokens = rawItemName.split(":");
            if(tokens.length == 0 || tokens.length > 3)
            {
               DiabolicalGems.LOGGER.warn("Invalid " + debugName + " entry: \"" + rawItemName + "\" - ignoring.");
               continue;
            }
            // Ore Dictionary (remember, no meta)
            else if(tokens[0].equals("ore") && tokens.length == 2)
            {
               for(ItemStack oreItemStack : OreDictionary.getOres(tokens[1]))
               {
                  if(oreItemStack.getMetadata() == OreDictionary.WILDCARD_VALUE)
                  {
                    
                  }
                  mapSet.put(oreItemStack.getItem(), oreItemStack.getMetadata());
               }            
            }
            else
            {
               int meta = 0;
               String itemName = tokens[0];
               if(tokens.length == 3)
               {
                  // This has to be metadata, or a wildcard
                  try
                  {
                     meta = Integer.parseInt(tokens[2]);
                     itemName = tokens[0] + ":" + tokens[1];
                  }
                  catch(@SuppressWarnings("unused") NumberFormatException e)
                  {
                     if(tokens[2].compareToIgnoreCase("*") == 0)
                     {
                        meta = OreDictionary.WILDCARD_VALUE;
                        itemName = tokens[0] + ":" + tokens[1];
                     }
                     else
                     {
                        DiabolicalGems.LOGGER.warn("Invalid " + debugName + " entry: \"" + rawItemName + "\" - ignoring.");
                        continue;
                     }
                  }               
               }
               else if(tokens.length == 2)
               {
                  // it might be a wild card metadata
                  if(tokens[1].compareToIgnoreCase("*") == 0)
                  {
                     meta = OreDictionary.WILDCARD_VALUE;
                     itemName = tokens[0];
                  }
                  else
                  {
                     // it might be metadata, or an item name
                     try
                     {
                        meta = Integer.parseInt(tokens[1]);
                        itemName = tokens[0];
                     }
                     catch(@SuppressWarnings("unused") NumberFormatException e)
                     {
                        itemName = tokens[0] + ":" + tokens[1];
                     }  
                  }
               }
               Item item = Item.REGISTRY.getObject(new ResourceLocation(itemName));
               if(item != null)
               {
                  mapSet.put(item, meta);
               }
               else
               {
                  DiabolicalGems.LOGGER.warn("Invalid " + debugName + " entry: \"" + rawItemName + "\" - ignoring.");               
               }
            }
         }
      }
   }
   
   @Config(comment="[modid:]name[:meta] - modid: none for \"minecraft:\", \"ore:\" for an Ore Dictionary entry. meta - none for \":0\", \":*\" for any. No meta with Ore Dictionary. If your modid is \"ore,\" may God have mercy on your soul.")
   String[] socketableWeapons = 
      {
         "ore:toolSword",
         "ore:toolAxe",
         "diamond_sword",
         "iron_sword",
         "golden_sword",
         "stone_sword",
         "wooden_sword",
         "diamond_axe",
         "iron_axe",
         "golden_axe",
         "stone_axe",
         "wooden_axe"
      }; 
   
   @Config(comment="[modid:]name[:meta] - modid: none for \"minecraft:\", \"ore:\" for an Ore Dictionary entry. meta - none for \":0\", \":*\" for any. No meta with Ore Dictionary. If your modid is \"ore,\" may God have mercy on your soul.")
   String[] socketableTools = 
      {
         "ore:toolAxe",
         "ore:toolShovel",
         "ore:toolHoe",
         "ore:toolShears",
         "diamond_axe",
         "iron_axe",
         "golden_axe",
         "stone_axe",
         "wooden_axe",
         "diamond_shovel",
         "iron_shovel",
         "golden_shovel",
         "stone_shovel",
         "wooden_shovel",
         "diamond_hoe",
         "iron_hoe",
         "golden_hoe",
         "stone_hoe",
         "wooden_hoe",
         "shears"
      };
   
   @Config(comment="[modid:]name[:meta] - modid: none for \"minecraft:\", \"ore:\" for an Ore Dictionary entry. meta - none for \":0\", \":*\" for any. No meta with Ore Dictionary. If your modid is \"ore,\" may God have mercy on your soul.")
   String[] socketableArmor = 
      {
         "ore:armorHelm",
         "ore:armorChestplate",
         "ore:armorLeggings",
         "ore:armorBoots",
         "diamond_boots",
         "iron_boots",
         "chainmail_boots",
         "golden_boots",
         "leather_boots",
         "diamond_leggings",
         "iron_leggings",
         "chainmail_leggings",
         "golden_leggings",
         "leather_leggings",
         "diamond_chestplate",
         "iron_chestplate",
         "chainmail_chestplate",
         "golden_chestplate",
         "leather_chestplate",
         "diamond_helmet",
         "iron_helmet",
         "chainmail_helmet",
         "golden_helmet",
         "leather_helmet"
      };
   
   @Config(comment="[modid:]name[:meta] - modid: none for \"minecraft:\", \"ore:\" for an Ore Dictionary entry. meta - none for \":0\", \":*\" for any. No meta with Ore Dictionary. If your modid is \"ore,\" may God have mercy on your soul.")
   String[] socketableShields = 
      {
         "ore:toolShield",
         "shield"
      };

   private final ItemMapSet weapons  =  new ItemMapSet();
   private final ItemMapSet tools    =  new ItemMapSet();
   private final ItemMapSet armor    =  new ItemMapSet();
   private final ItemMapSet shields  =  new ItemMapSet();
   
   GemUseHandler()
   {
      DiabolicalGems.CONFIG.register(this);
      syncConfig();
   }

   void syncConfig()
   {
      weapons.clear();
      weapons.populate(socketableWeapons, "socketableWeapons");
      
      tools.clear();
      tools.populate(socketableTools, "socketableTools");
      
      armor.clear();
      armor.populate(socketableArmor, "socketableArmor");
      
      shields.clear();
      shields.populate(socketableShields, "socketableShields");
   }
   
   boolean canUseGemOnItem(ItemStack itemStack, GemSet gemSet)
   {
      Item item = itemStack.getItem();
      int meta = item.getHasSubtypes() ? itemStack.getMetadata() : 0;
      //! TODO I'm sure this could be better:
      return (weapons.containsValue(item, meta) && ArrayUtils.contains(gemSet.getValidUses().toArray(), GemUseTypes.WEAPON))   ||
             (tools.containsValue(item, meta)   && ArrayUtils.contains(gemSet.getValidUses().toArray(), GemUseTypes.TOOL))     ||
             (armor.containsValue(item, meta)   && ArrayUtils.contains(gemSet.getValidUses().toArray(), GemUseTypes.ARMOR))    ||
             (shields.containsValue(item, meta) && ArrayUtils.contains(gemSet.getValidUses().toArray(), GemUseTypes.SHIELD))   ;
   }

}
