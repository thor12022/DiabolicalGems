package thor12022.diabolicalgems.core;

import java.util.ArrayList;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.IGemSet;

class GemSet implements IGemSet
{
   final private EnchantmentGemSet           enchantment;
   final private ItemGemSet                  item;
   final private String                      gemType;
   final private ArrayList<GemUseTypes>      validUses;
   
   
   public GemSet(String gemType, int color, ArrayList<GemUseTypes> validUses)
   {
      this.validUses =  validUses;
      this.gemType   =  gemType;
      item           =  new ItemGemSet(gemType, color);
      enchantment    =  new EnchantmentGemSet(gemType);
   }
   
   Enchantment getEnchantment()
   {
      return enchantment;
   }
   
   @Override
   public Item getItem()
   {
      return item;
   }   

   @Override
   public String getName()
   {
      return gemType;
   }
   
   public ItemStack getGem(GemQuality type)
   {
      return new ItemStack(item, 1, type.toMeta());
   }

   public ArrayList<GemUseTypes> getValidUses()
   {
      return validUses;
   }
}
