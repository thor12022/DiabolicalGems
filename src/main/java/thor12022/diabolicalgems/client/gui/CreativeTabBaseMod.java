package thor12022.diabolicalgems.client.gui;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.ModInformation;

public class CreativeTabBaseMod extends CreativeTabs
{

   public CreativeTabBaseMod(String tabLabel)
   {
      super(tabLabel);
      setBackgroundImageName(ModInformation.ID + ".png"); // Automagically has
                                                          // tab_ applied to it.
                                                          // Make sure you
                                                          // change the texture
                                                          // name.
   }

   @Override
   public boolean hasSearchBar()
   {
      return false;
   }

   // The tab icon is what you return here.
   @Override
   public ItemStack getIconItemStack()
   {
      return DiabolicalGems.API.getCreativeTabIcon();
   }

   @Override
   public Item getTabIconItem()
   {
      return new Item();
   }
}
