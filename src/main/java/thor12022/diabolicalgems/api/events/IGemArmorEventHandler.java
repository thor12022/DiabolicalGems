package thor12022.diabolicalgems.api.events;

import javax.annotation.Nonnull;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.api.GemQuality;

public interface IGemArmorEventHandler extends IGemEventHandler
{

   /**
    * 
    * @param source
    * @param target
    * @param quality
    * @param event the LivingHurtEvent (for any additional needed information)
    * @note event processing will respect cancelled events and cease
    */
   void onHit(@Nonnull DamageSource source, @Nonnull EntityLivingBase target, @Nonnull GemQuality quality, @Nonnull LivingHurtEvent event);
}
