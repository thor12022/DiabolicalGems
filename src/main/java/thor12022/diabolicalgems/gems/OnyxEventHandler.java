package thor12022.diabolicalgems.gems;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.events.IGemArmorEventHandler;
import thor12022.diabolicalgems.config.Config;
import thor12022.diabolicalgems.config.Configurable;

@Configurable(sectionName="Onyx")
class OnyxEventHandler extends AbstractGemSetEventHandler implements IGemArmorEventHandler
{
   @Config(minFloat = 0)
   private float healthStealingMultiplier = 0.5f;
   
   OnyxEventHandler()
   {
      //! @todo is this doing it right?
      super(GemType.ONYX, Potion.getPotionFromResourceLocation("wither"));
   }
   
   @Override
   public void onHit(DamageSource source, EntityLivingBase target, GemQuality quality, LivingHurtEvent event)
   {
      if(!target.getEntityWorld().isRemote && 
         source.getEntity() instanceof EntityLivingBase)
      {
         EntityLivingBase entitySource = (EntityLivingBase)event.getSource().getEntity();
                  
         float health = Math.abs((float)DiabolicalGems.RAND.nextGaussian() * (quality.toMeta() + 1) * healthStealingMultiplier);
         if(entitySource.attackEntityFrom(DamageSource.magic, health))
         {
            target.heal(health);
         }
      }
   }
}
