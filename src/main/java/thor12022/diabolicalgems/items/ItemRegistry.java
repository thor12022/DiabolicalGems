package thor12022.diabolicalgems.items;

import java.lang.reflect.Field;
import java.util.ArrayList;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import thor12022.diabolicalgems.DiabolicalGems;

public class ItemRegistry
{
   private ArrayList<IItem> items = new ArrayList<>();
   
   public ItemRegistry()
   {
      // Collect all the declared items into a list for easy registering
      for( Field field : getClass().getFields())
      {
         if(field.getType() == Item.class)
         {
            try
            {
               //Checks if it is castable to IItem and static
               items.add((IItem)field.get(null));
            }
            catch(@SuppressWarnings("unused") Exception e)
            {
               DiabolicalGems.LOGGER.debug("Item not properly defined in registry: " + field.getName());
            }
         }
      }
   }
   
   /**
    * 
    * @param item must not have been registered
    * @return false if cannot be added, or duplicate
    * @note Server must be prior to Constructing state
    */
   public boolean addItem(IItem item)
   {
      if(!Loader.instance().hasReachedState(LoaderState.PREINITIALIZATION) && item != null)
      {
         return items.add(item);
      }
      return false;
   }
   
   public void registerItems()
   {
      for(IItem item : items)
      {
         if(item.isEnabled())
         {
            item.registerItem();
         }
      }
   }
   
   @SideOnly(Side.CLIENT)
   public void registerModels()
   {
      for(IItem item : items)
      {
         if(item.isEnabled())
         {
            item.registerModel();
         }
      }
   }
   
   public void registerRecipes()
   {
      for(IItem item : items)
      {
         if(item.isEnabled())
         {
            item.registerRecipe();
         }
      }
   }
}
