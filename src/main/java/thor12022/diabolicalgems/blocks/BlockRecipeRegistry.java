package thor12022.diabolicalgems.blocks;

public class BlockRecipeRegistry
{

   // Self explanatory. Continue these how you wish. EG:
   // registerPulverizerRecipes
   private static void registerShapedRecipes()
   {
	   
   }

   private static void registerShaplessRecipes()
   {

   }

   public static void registerBlockRecipes()
   {
      registerShapedRecipes();
      registerShaplessRecipes();
   }
}
