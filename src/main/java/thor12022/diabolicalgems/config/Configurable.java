package thor12022.diabolicalgems.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Configurable
{
   public final String SECTION_GENERAL = "General";
   
   String sectionName() default "";
   
   String syncNotification() default "";
}
