# Diabolical Gems

Gems combined with weapons/armour give them special abilities.

Step 1: Get socketed items from mob drops
Step 2: Get gems from mob drops
Step 3: Combine gems and weapons in anvil
Step 4: ?
Step 5: Profit.


Feel free to use in modpacks; notification is appreciated, but not required.
