package thor12022.diabolicalgems.potion;

import net.minecraft.potion.Potion;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class PotionRegistry
{

   public static Potion potionBurning        = new PotionBurning();
   public static Potion potionDrowning       = new PotionDrowning();
   public static Potion potionLightningRod   = new PotionLightningRod();
   public static Potion potionSuffocation    = new PotionSuffocation();

   public PotionRegistry()
   {}
   
   public void registerPotions()
   {
      GameRegistry.register(potionBurning);
      GameRegistry.register(potionDrowning);
      GameRegistry.register(potionLightningRod);
      GameRegistry.register(potionSuffocation);
   }
}