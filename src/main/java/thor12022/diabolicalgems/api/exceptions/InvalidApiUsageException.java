package thor12022.diabolicalgems.api.exceptions;

/**
 * Thrown if the Dialbolical Gems API is used improperly, e.g. called before being initialized
 *
 */
public class InvalidApiUsageException extends DiabolicalGemsException
{
  private static final long serialVersionUID = -2948299972241563384L;

   public InvalidApiUsageException(String string)
   {
      super("Invalid API Usage: " + string);
   }

}
