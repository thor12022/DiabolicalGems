package thor12022.diabolicalgems.gems;

import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.util.ColorHelpers;

enum GemType
{
   AMETHYST    ("amethyst",   AmethystEventHandler.class,   ColorHelpers.fromRgb(102, 32, 98)),
   AZURITE     ("azurite",    AzuriteEventHandler.class,    ColorHelpers.fromRgb(26, 34, 203)),
   GARNET      ("garnet",     GarnetEventHandler.class,     ColorHelpers.fromRgb(213, 2, 0)),
   ONYX        ("onyx",       OnyxEventHandler.class,       ColorHelpers.fromRgb(4, 1, 0)),
   TOPAZ       ("topaz",      TopazEventHandler.class,      ColorHelpers.fromRgb(225, 221, 62)),
   TORBERNITE  ("torbernite", TorberniteEventHandler.class, ColorHelpers.fromRgb(17, 73, 32));
     
   public static int number()
   {
      return values().length;
   }
   
   public static GemType random()
   {
      return values()[DiabolicalGems.RAND.nextInt(values().length)];
   }
   
   private final String name;
   private final Class<? extends AbstractGemSetEventHandler> eventHandler;
   private final int color;
   
   private GemType(String name, Class<? extends AbstractGemSetEventHandler> eventHandler, int color)
   {
      this.name = name;
      this.eventHandler = eventHandler;
      this.color = color;
   }
   
   public Class<? extends AbstractGemSetEventHandler> getEventHandler()
   {
      return eventHandler;
   }
   
   public int getColor()
   {
      return color;
   }
   
   @Override
   public String toString()
   {
      return name;
   }
}
