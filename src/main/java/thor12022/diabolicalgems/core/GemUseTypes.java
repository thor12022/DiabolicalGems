package thor12022.diabolicalgems.core;

enum GemUseTypes
{
   SHIELD,
   WEAPON,
   TOOL,
   ARMOR;
   
   public static final GemUseTypes[] ALL = 
      {
         SHIELD,
         WEAPON,
         TOOL,
         ARMOR
      };
   
   public static final GemUseTypes[] MAIN_HAND = 
      {
         WEAPON,
         TOOL
      };
}
