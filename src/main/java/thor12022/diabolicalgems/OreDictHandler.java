package thor12022.diabolicalgems;

import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class OreDictHandler
{
   // Oredict entries
   public static String blockObsidian = "blockObsidian";

   public static void registerOreDict()
   {
     if(!OreDictionary.doesOreNameExist(blockObsidian))
     {
        OreDictionary.registerOre(blockObsidian, new ItemStack(Blocks.OBSIDIAN, 1, 0));
     }
   }
}