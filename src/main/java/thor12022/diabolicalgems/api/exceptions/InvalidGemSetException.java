package thor12022.diabolicalgems.api.exceptions;

/**
 * Thrown when there is something wrong with the implementation of the Gem Set
 *    e.g. Does not implement any uses
 */
public class InvalidGemSetException extends DiabolicalGemsException
{
   private static final long serialVersionUID = 1364732455030226622L;

   public InvalidGemSetException(String gemSet, String problem)
   {
      super( "Gem set " + gemSet + " is not properly implemented: " + problem);
   }

}
