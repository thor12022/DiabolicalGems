package thor12022.diabolicalgems.util;

import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.ModInformation;

public class EventHandler
{
   @SubscribeEvent
   public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent eventArgs)
   {
      if(eventArgs.getModID().equals(ModInformation.ID))
      {
         DiabolicalGems.CONFIG.syncConfig();
         DiabolicalGems.LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.config.refresh"));
      }
   }
}
