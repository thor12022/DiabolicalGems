package thor12022.diabolicalgems.potion;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.config.Configurable;
import thor12022.diabolicalgems.util.ColorHelpers;

@Configurable
public class PotionSuffocation extends AbstractPotion
{
   private static final String   NAME        =  "suffocation";
   private static final int      COLOR       =  ColorHelpers.fromRgb(0xFF, 0x00, 0xFF);
   private static final int      ICON_INDEX  =  5;

   protected PotionSuffocation()
   {
      super(NAME, false, COLOR, ICON_INDEX);
      DiabolicalGems.CONFIG.register(this);
   }
   
   @Override
   public void performEffect(EntityLivingBase entityLivingBaseIn, int level)
   {
      if(!(entityLivingBaseIn instanceof EntityPlayer && 
          ((EntityPlayer)entityLivingBaseIn).capabilities.disableDamage))
      {
         entityLivingBaseIn.attackEntityFrom(DamageSource.inWall, 1.0F);
      }
   }
}
