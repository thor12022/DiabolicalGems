package thor12022.diabolicalgems.gems;

import thor12022.diabolicalgems.potion.PotionRegistry;

class AmethystEventHandler extends AbstractGemSetEventHandler
{

   AmethystEventHandler()
   {
      super(GemType.AMETHYST, PotionRegistry.potionSuffocation);
   }
   
}
