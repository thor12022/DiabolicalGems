package thor12022.diabolicalgems.api.events;

import javax.annotation.Nonnull;

import net.minecraft.entity.EntityLivingBase;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.api.GemQuality;

public interface IGemWeaponEventHandler extends IGemEventHandler
{
   /**
    * 
    * @param attacker the one wielding the Gemmed weapon
    * @param target the one getting hit with said weapon
    * @param quality the quality of the relevant gem on the weapon
    * @param event the LivingHurtEvent (for any additional needed information)
    * @note event processing will respect cancelled events and cease
    */
   void onAttack(@Nonnull EntityLivingBase attacker, @Nonnull EntityLivingBase target, @Nonnull GemQuality quality, @Nonnull LivingHurtEvent event);
}
