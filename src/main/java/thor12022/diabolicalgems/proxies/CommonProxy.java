package thor12022.diabolicalgems.proxies;

import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.OreDictHandler;
import thor12022.diabolicalgems.blocks.BlockRecipeRegistry;
import thor12022.diabolicalgems.blocks.BlockRegistry;
import thor12022.diabolicalgems.gems.GemHelper;
import thor12022.diabolicalgems.items.ItemRegistry;
import thor12022.diabolicalgems.potion.PotionRegistry;

public class CommonProxy
{
   protected ItemRegistry     itemRegistry   =  new ItemRegistry();
   protected GemHelper        gemHelper      =  new GemHelper();
   protected PotionRegistry   potionRegistry =  new PotionRegistry();

   public void preInit()
   {
      itemRegistry.registerItems();
      potionRegistry.registerPotions();
      DiabolicalGems.API.register();
      BlockRegistry.registerBlocks();
   }

   public void init()
   {
      OreDictHandler.registerOreDict();
      itemRegistry.registerRecipes();
      BlockRecipeRegistry.registerBlockRecipes();
   }
   
   public void postInit()
   {
      DiabolicalGems.API.postInit();
   }
}
