package thor12022.diabolicalgems.gems;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.events.IGemArmorEventHandler;
import thor12022.diabolicalgems.config.Configurable;

@Configurable(sectionName="Torbernite")
class TorberniteEventHandler extends AbstractGemSetEventHandler implements IGemArmorEventHandler
{
   //! @todo is this doing it right?
   final private static Potion POISON_POTION = Potion.getPotionFromResourceLocation("poison");
   
   TorberniteEventHandler()
   {
      super(GemType.TORBERNITE, POISON_POTION);
   }
   
   /**
    * This will decrease the duration of the Poison Potion Effect
    *    
    * @todo I would prefer to reduce the duration once upon application, but I don't know if that is feasible
    */
   @Override
   public void onHit(DamageSource source, EntityLivingBase target, GemQuality quality, LivingHurtEvent event)
   {
      if(!event.getEntity().getEntityWorld().isRemote &&
         source.isMagicDamage() &&  //! TODO need a better way of determining Poison Damage
         target.isPotionActive(POISON_POTION))
      {
         PotionEffect effect = target.getActivePotionEffect(POISON_POTION);
         target.removeActivePotionEffect(POISON_POTION);
         target.addPotionEffect(new PotionEffect(POISON_POTION, effect.getDuration() - quality.toMeta() - 1, effect.getAmplifier()));
      }
   }
}
