package thor12022.diabolicalgems.potion;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.util.DamageSource;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.util.ColorHelpers;

public class PotionDrowning extends AbstractPotion
{
   private static final String   NAME        =  "drowning";
   private static final int      COLOR       =  ColorHelpers.fromRgb(0x33, 0x33, 0x99);
   private static final int      ICON_INDEX  =  2;
   
   PotionDrowning()
   {
      super(NAME, true, COLOR, ICON_INDEX);
      DiabolicalGems.CONFIG.register(this);
   }
   
   @Override
   public void performEffect(EntityLivingBase entityLivingBaseIn, int level)
   {
      if(!entityLivingBaseIn.canBreatheUnderwater() && 
         !entityLivingBaseIn.isPotionActive(MobEffects.WATER_BREATHING) &&
         !(entityLivingBaseIn instanceof EntityPlayer && 
          ((EntityPlayer)entityLivingBaseIn).capabilities.disableDamage))
      {
         entityLivingBaseIn.attackEntityFrom(DamageSource.drown, 1.0F);
      }
      
      if(!entityLivingBaseIn.getEntityWorld().isRemote)
      {
         /*BlockPos pos = entityLivingBaseIn.getPosition();
         if(entityLivingBaseIn.getEntityWorld().isAirBlock(pos.add(0.5, 0, 0.5)) && 
            entityLivingBaseIn.getEntityWorld().isSideSolid(pos.add(0.5, -0.5, 0.5), EnumFacing.UP))
         {
            entityLivingBaseIn.getEntityWorld().setBlockState(pos, Blocks.water.getStateFromMeta(7), 2);
         }*/
      }
      else
      {
         // just spawn some bubbles instead
      }
   }
}
