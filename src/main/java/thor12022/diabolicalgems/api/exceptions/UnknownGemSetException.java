package thor12022.diabolicalgems.api.exceptions;

/**
 * Thrown when the gem set does not exist
 */
public class UnknownGemSetException extends DiabolicalGemsException
{
   private static final long serialVersionUID = 1364732455030226622L;

   public UnknownGemSetException(String gemSet)
   {
      super("Invalid Gem Set: " + gemSet);
   }

}
