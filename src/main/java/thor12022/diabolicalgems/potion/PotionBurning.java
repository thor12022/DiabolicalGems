package thor12022.diabolicalgems.potion;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.util.ColorHelpers;

public class PotionBurning extends AbstractPotion
{
   private static final String   NAME        =  "burning";
   private static final int      COLOR       =  ColorHelpers.fromRgb(0xFF, 0x66, 0x00);
   private static final int      ICON_INDEX  =  4;
   
   protected PotionBurning()
   {
      super(NAME, true, COLOR, ICON_INDEX);
      DiabolicalGems.CONFIG.register(this);
   }
   
   @Override
   public void performEffect(EntityLivingBase entityLivingBaseIn, int level)
   {
      if(!entityLivingBaseIn.isImmuneToFire() && 
         !(entityLivingBaseIn instanceof EntityPlayer && 
          ((EntityPlayer)entityLivingBaseIn).capabilities.disableDamage))
      {
         entityLivingBaseIn.attackEntityFrom(DamageSource.onFire, 1.0F);
      }
   }
}
