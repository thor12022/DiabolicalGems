package thor12022.diabolicalgems.gems;

import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.api.exceptions.DiabolicalGemsException;

public class GemHelper
{   
   public GemHelper()
   {
      for(GemType gem : GemType.values())
      {
         try
         {
            DiabolicalGems.API.createGemSet(gem.toString(), gem.getColor(), gem.getEventHandler().newInstance());
         }
         catch(DiabolicalGemsException | InstantiationException | IllegalAccessException e)
         {
            DiabolicalGems.LOGGER.debug(e);
            DiabolicalGems.LOGGER.warn("Cannot create Gem Set: " + gem);
         }
      }
   }

}
