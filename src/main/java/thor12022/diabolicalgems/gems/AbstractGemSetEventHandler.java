package thor12022.diabolicalgems.gems;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.events.IGemWeaponEventHandler;
import thor12022.diabolicalgems.config.Config;

class AbstractGemSetEventHandler implements IGemWeaponEventHandler
{
   @Config(minFloat = 0)
   protected float weaponDamageTickTimeMultiplier = 1.5f;

   @Config(minInt = 0)
   protected int weaponDamageBaseTickTime = 20;

   protected GemType       gemType;
   protected Potion        weaponPotion;

   AbstractGemSetEventHandler(GemType gemType, Potion weaponPotion)
   {
      this.gemType = gemType;
      this.weaponPotion = weaponPotion;
      DiabolicalGems.CONFIG.register(this);
      // Only register if there's a reason for the event
      if(weaponPotion != null)
      {
         MinecraftForge.EVENT_BUS.register(this);
      }
   }
   
   int weaponDamageTicks(GemQuality quality)
   {
      return weaponDamageBaseTickTime * (int)(weaponDamageTickTimeMultiplier * (1 + quality.toMeta()));
   }
   
   @Override
   public void onAttack(EntityLivingBase attacker, EntityLivingBase target, GemQuality quality, LivingHurtEvent event)
   {
      if(weaponPotion != null && 
         !event.getEntity().getEntityWorld().isRemote)
      {
         
         PotionEffect poisonEffect = new PotionEffect(weaponPotion, weaponDamageTicks(quality));
         event.getEntityLiving().addPotionEffect(poisonEffect);
      }
   }
}
