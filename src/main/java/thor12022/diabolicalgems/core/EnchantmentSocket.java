package thor12022.diabolicalgems.core;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.item.ItemStack;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.ModInformation;
import thor12022.diabolicalgems.config.Config;
import thor12022.diabolicalgems.config.Configurable;
import thor12022.diabolicalgems.util.EquipmentHelper;

@Configurable(sectionName = "Socket")
class EnchantmentSocket extends Enchantment
{
	static final String NAME = "socket";

	@Config(minInt = 0)
	static int maxSockets = 3;
   
	static
	{
		DiabolicalGems.CONFIG.register(EnchantmentSocket.class);
	}

	EnchantmentSocket()
	{
		super(Enchantment.Rarity.VERY_RARE, EnumEnchantmentType.ALL, EquipmentHelper.ALL_SLOTS);
		this.setName(ModInformation.ID + "." + NAME);
      this.setRegistryName(NAME);     
      DiabolicalGems.CONFIG.register(this);  
	}

	@Override
	public boolean canApply(ItemStack stack)
	{
		return stack.isItemEnchantable();
	}

	@Override
	public boolean canApplyTogether(Enchantment enchant)
	{
		return enchant != this;
	}

	@Override
	public boolean isAllowedOnBooks()
	{
		return false;
	}

	@Override
	public int getMaxLevel()
	{
		return maxSockets;
	}
	
   @Override
   public boolean isTreasureEnchantment()
   {
      //! @todo figure out what this is used for
      return true;
   }

   @Override
   public boolean canApplyAtEnchantingTable(ItemStack stack)
   {
      return true;
   }
}