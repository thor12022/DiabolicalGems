package thor12022.diabolicalgems.util;

import net.minecraft.inventory.EntityEquipmentSlot;

public class EquipmentHelper
{
	public static final EntityEquipmentSlot[] ALL_SLOTS = 
		{
			EntityEquipmentSlot.MAINHAND, 
			EntityEquipmentSlot.OFFHAND, 
			EntityEquipmentSlot.FEET,
			EntityEquipmentSlot.LEGS,
			EntityEquipmentSlot.CHEST,
			EntityEquipmentSlot.HEAD
		};
}
