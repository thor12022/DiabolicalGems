package thor12022.diabolicalgems.gems;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.events.IGemArmorEventHandler;
import thor12022.diabolicalgems.config.Configurable;
import thor12022.diabolicalgems.potion.PotionRegistry;

@Configurable(sectionName="Azurite")
class AzuriteEventHandler extends AbstractGemSetEventHandler implements IGemArmorEventHandler
{

   AzuriteEventHandler()
   {
      super(GemType.AZURITE, PotionRegistry.potionDrowning);
   }
   
   @Override
   public void onHit(DamageSource source, EntityLivingBase target, GemQuality quality, LivingHurtEvent event)
   {
      if(!target.worldObj.isRemote &&
          source == DamageSource.drown)
      {
         if(event.getAmount() > 1)
         {
            //TODO add in a bit of a random factor here
            event.setAmount(event.getAmount() * (1.0f - ((quality.toMeta()+1) / (float)GemQuality.number())));
            if(event.getAmount() == 0)
            {
               event.setCanceled(true);
            }
         }
         else
         {
            int bound = GemQuality.number() - quality.toMeta() - 1;
            if(bound == 0 ||
               DiabolicalGems.RAND.nextInt(bound) ==  0)
            {
               event.setCanceled(true);
            }
         }
      }
   }
}
