package thor12022.diabolicalgems.potion;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.config.Configurable;
import thor12022.diabolicalgems.util.ColorHelpers;

@Configurable
public class PotionLightningRod extends AbstractPotion
{
   private static final String   NAME        = "lightningRod";
   private static final int      COLOR       = ColorHelpers.fromRgb(0xE6, 0xFF, 0xFF);
   private static final int      ICON_INDEX  = 3;

   PotionLightningRod()
   {
      super(NAME, true, COLOR, ICON_INDEX);
      DiabolicalGems.CONFIG.register(this);
   }
   
   @Override
   public void performEffect(EntityLivingBase entityLivingBaseIn, int level)
   {
      if(!(entityLivingBaseIn instanceof EntityPlayer && 
          ((EntityPlayer)entityLivingBaseIn).capabilities.disableDamage))
      {
         entityLivingBaseIn.attackEntityFrom(DamageSource.lightningBolt, 1.0F);
      }
   }
}
