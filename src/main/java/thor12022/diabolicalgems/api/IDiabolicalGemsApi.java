package thor12022.diabolicalgems.api;

import java.util.Collection;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import thor12022.diabolicalgems.api.events.IGemEventHandler;
import thor12022.diabolicalgems.api.exceptions.InvalidApiUsageException;
import thor12022.diabolicalgems.api.exceptions.InvalidGemSetException;
import thor12022.diabolicalgems.api.exceptions.UnknownGemSetException;

/**
 * This should not be implemented
 */
public interface IDiabolicalGemsApi
{
   /**
    * @param gemName
    * @param color
    * @param validUses
    * @param eventHandler
    * @return a new IGemSet if the gemName is unique, null otherwise
    * @throws InvalidApiUsageException if FML is not in CONSTUCTING
    * @throws InvalidGemSetException if no valid uses for gemset
    */
   <EventHandlerClass extends IGemEventHandler>
   IGemSet createGemSet(String gemName, int color, EventHandlerClass eventHandler) throws InvalidApiUsageException, InvalidGemSetException;
   
   /**
    * @param gemName
    * @return the IGemSet for gemName, null if it does not exist
    * @note should called PREINITIALIZATION or later to ensure all gemsets are present
    */
   IGemSet getGemSet(String gemName);
   
   /**
    * @param gemName
    * @return all IGemSets
    * @note should called PREINITIALIZATION or later to ensure all gemsets are present
    */
   Collection<? extends IGemSet> getGemSets();
   
   /**
    * @param gemName
    * @param stack
    * @return the GemQuality of gemName on the stack, null if gemName is not on stack
    * @throws UnknownGemSetException if gemName does not exist
    * @todo maybe just returning null instead of throwing would be better?
    */
   GemQuality getSocketedGemQuality(String gemName, ItemStack stack) throws UnknownGemSetException;
   
   /**
    * The number of sockets available on the ItemStack
    * @param stack
    */
   int getEmptySockets(ItemStack stack);
   
   /**
    * Gets the combined gem quality of a specific gemset on a specific entity. 
    *    For exammple if there are 2 Chipped gems, the quality retuned would be Flawed
    * @param gemName
    * @param entity the EntityLivingBase in question
    * @throws UnknownGemSetException if the gemName does not exist
    */
   GemQuality getCombinedArmorGemQuality(EntityLivingBase entity, String gemName) throws UnknownGemSetException;
}
