package thor12022.diabolicalgems.util;

public class ColorHelpers
{
   /**
    * 
    * @param r Red, 0-255, only the 8 LSBs used
    * @param g Green, 0-255, only the 8 LSBs used
    * @param b Blue, 0-255, only the 8 LSBs used
    * @return integer value of color with assumed alpha of 0x00
    */
   public static int fromRgb(int r, int g, int b)
   {
      return ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff);
   }

   /**
    * 
    * @param r Red, 0-255, only the 8 LSBs used
    * @param g Green, 0-255, only the 8 LSBs used
    * @param b Blue, 0-255, only the 8 LSBs used
    * @param b Alpha, 0-255, only the 8 LSBs used
    * @return integer value of color
    */
   public static int fromRgba(int r, int g, int b, int a)
   {
      return ((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff) | ((a &0xff) << 24);
   }

}
