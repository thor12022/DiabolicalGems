package thor12022.diabolicalgems.core;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import thor12022.diabolicalgems.ModInformation;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.items.AbstractItem;
import thor12022.diabolicalgems.util.TextHelper;

class ItemGemSet extends AbstractItem
{
   private final static String NAME = "gem_set";
   
   final private String gemType;
   final private String modOwner = Loader.instance().activeModContainer().getModId();
   final private int color;
   
   ItemGemSet(String gemType, int color)
   {
      super(NAME + "_" + gemType);
      this.gemType = gemType;
      this.color = color;
      this.setHasSubtypes(true);
   }

   static GemQuality getGemQuality(ItemStack stack)
   {
      if(stack.getItem() instanceof ItemGemSet)
      {
         return GemQuality.fromMeta(((ItemGemSet)stack.getItem()).getMetadata(stack));
      }
      return null;
   }
   
   public String getGemType()
   {
      return gemType;
   }

   @Override
   public String getUnlocalizedName(ItemStack stack)
   {
      if(stack.getMetadata() < GemQuality.number())
      {
         return "item." + modOwner + ".gem_" + gemType + "_" + GemQuality.values()[stack.getMetadata()];
      }
      return null;
   }
   
   @Override
   public String getItemStackDisplayName(ItemStack stack)
   {
      String gemDisplayName =  TextHelper.localize("gem." + modOwner + "." + ((ItemGemSet)stack.getItem()).getGemType());
      return TextHelper.localize("quality." + ModInformation.ID + "." + ItemGemSet.getGemQuality(stack), gemDisplayName);
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public void getSubItems(Item item, CreativeTabs creativeTab, List<ItemStack> itemStacks)
   {
      for(int i = 0; i < GemQuality.values().length; ++i)
      {
         itemStacks.add(new ItemStack(item, 1, i));
      }
   }
   
   
   @Override
   @SideOnly(Side.CLIENT)
   public boolean hasEffect(ItemStack stack)
   {
       return false;
   }

   @Override
   public void registerModel()
   {
      for(int i = 0; i< GemQuality.number(); ++i)
      {
         ModelLoader.setCustomModelResourceLocation(this, i, new ModelResourceLocation(ModInformation.ID + ":gem", "quality=" + GemQuality.values()[i]));
      }
   }
   
   public void registerModelColors()
   {
      Minecraft.getMinecraft().getItemColors().registerItemColorHandler(
         new IItemColor()
         {
            @Override
            public int getColorFromItemstack(ItemStack stack, int tintIndex)
            {
               return tintIndex > 0 ? -1 : color;
            }
         },
         new Item[] {this});
   }
   
   @Override
   public void registerRecipe()
   {}
   
   @Override
   public boolean isEnabled()
   {
      return true;
   }

}
