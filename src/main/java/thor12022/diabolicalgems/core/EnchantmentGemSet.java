package thor12022.diabolicalgems.core;

import net.minecraftforge.fml.common.Loader;
import thor12022.diabolicalgems.DiabolicalGems;

class EnchantmentGemSet extends AbstractGemSetEnchantment
{
   protected static final String NAME = "gem";
      
   static
   {
      DiabolicalGems.CONFIG.register(EnchantmentSocket.class);
   }
   
   EnchantmentGemSet(String gemType)
   {
      super();
      this.setName(Loader.instance().activeModContainer().getModId() + "." + gemType);
      this.setRegistryName(NAME + "_" + gemType);
   }
   
   @Override
   public String getName()
   {
       return "gem." + this.name;
   }

}
