package thor12022.diabolicalgems.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MapSet<Key, Value>
{
   private final Map<Key, Set<Value>> mapSet= new HashMap<>();
   
   public int put(Key key, Value value)
   {
      if(!mapSet.containsKey(key))
      {
         mapSet.put(key, new HashSet<>());
      }
      mapSet.get(key).add(value);
      return mapSet.get(key).size();
   }
   
   public boolean containsKey(Key key)
   {
      return mapSet.containsKey(key);
   }
   
   public boolean containsValue(Key key, Value value)
   {
      return mapSet.containsKey(key) ? mapSet.get(key).contains(value) : false;
   }
   
   public void clear()
   {
      mapSet.clear();
   }
}
