package thor12022.diabolicalgems.core;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.item.ItemStack;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.util.EquipmentHelper;

/**
 * The Enchantment that Gem Sets Enchantments should be based upon
 * This does not allow for the Enchant to be used on a book, in an anvil, or at an Enchanting Table.<BR>
 * Four methods for events are provided, note that this does not prevent the implementor from
 * using their own Event Handler for additional functionality.<BR>
 * Gem Set Enchants can be applied to all items that can have Sockets, Armour, Weapons, Tools & (enchantable)Shields,
 * and should have some affect for all of these items 
 */
abstract class AbstractGemSetEnchantment extends Enchantment
{
   protected AbstractGemSetEnchantment()
   {
      super(Enchantment.Rarity.VERY_RARE, EnumEnchantmentType.ALL, EquipmentHelper.ALL_SLOTS);
   }
   
   @Override
   final public boolean canApply(ItemStack stack)
   {
      return false;
   }

   @Override
   final public boolean canApplyTogether(Enchantment enchant)
   {
      return false;
   }

   @Override
   final public boolean isAllowedOnBooks()
   {
      return false;
   }

   @Override
   final public int getMaxLevel()
   {
      return GemQuality.number();
   }

   @Override
   final public boolean isTreasureEnchantment()
   {
      return false;
   }

   @Override
   final public boolean canApplyAtEnchantingTable(ItemStack stack)
   {
      return false;
   }

}
