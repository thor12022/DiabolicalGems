package thor12022.diabolicalgems;

/*
 * Basic information your mod depends on.
 */

public class ModInformation
{

   public static final String NAME = "Diabolical Gems";
   public static final String ID = "diabolicalgems";
   public static final String CHANNEL = "DiabolicalGems";
   public static final String DEPEND = "required-after:Forge@*";
   public static final String VERSION = "%VERSION%";
   public static final String CLIENTPROXY = "thor12022.diabolicalgems.proxies.ClientProxy";
   public static final String COMMONPROXY = "thor12022.diabolicalgems.proxies.CommonProxy";
}
