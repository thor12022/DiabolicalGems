package thor12022.diabolicalgems.items;

import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.ModInformation;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;

public abstract class AbstractItem extends Item implements IItem
{

   String name;
   
   public AbstractItem(String name)
   {
      super();
      this.name = name;
      setUnlocalizedName(ModInformation.ID + "." + name);
      setRegistryName(new ResourceLocation(ModInformation.ID + ":" + name));
      setCreativeTab(DiabolicalGems.CREATIVE_TAB);
   }
   
   @Override
   public void registerItem()
   {
      GameRegistry.register(this);
   }

   @Override
   public void registerModel()
   {
      ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(ModInformation.ID + ":" + name()));
   }
   
   @Override
   public String name()
   {
      return name;
   }
}
