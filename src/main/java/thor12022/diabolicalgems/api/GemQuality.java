package thor12022.diabolicalgems.api;

public enum GemQuality
{
   CHIPPED("chipped"),
   FLAWED("flawed"),
   NORMAL("normal"),
   FLAWLESS("flawless"),
   PERFECT("perfect");
   
   public static int number()
   {
      return values().length;
   }
   
   /**
    *  @return the appropriate GemQuality for meta int, 
    *    will cut off at hightest meta value
    *  @todo handle negative inputs
   **/
   public static GemQuality fromMeta(int meta)
   {
      if(meta > 0)
      {
         return values()[meta < number() ? meta : number() - 1]; 
      }
      return values()[0];
   }
   
   private String name;
   
   private GemQuality(String name)
   {
      this.name = name;
   }   
   
   public int toMeta()
   {
      return this.ordinal();
   }
   
   public boolean isBetterThan(GemQuality gemQuality)
   {
      return this.ordinal() > gemQuality.ordinal(); 
   }
   
   public GemQuality getNextBetter()
   {
       return this != GemQuality.PERFECT ? fromMeta(this.ordinal() + 1) : GemQuality.PERFECT;
   }
   
   public boolean isWorseThan(GemQuality gemQuality)
   {
      return this.ordinal() < gemQuality.ordinal(); 
   }
   
   
   @Override
   public String toString()
   {
      return name;
   }
}
