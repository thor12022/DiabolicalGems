package thor12022.diabolicalgems.api;

import net.minecraft.item.Item;

public interface IGemSet
{
   public Item getItem();
   public String getName();
}
