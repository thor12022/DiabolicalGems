package thor12022.diabolicalgems.api.exceptions;

public class DiabolicalGemsException extends Exception
{
   private static final long serialVersionUID = 1L;
   
   public DiabolicalGemsException(String string)
   {
      super(string);
   }
}
