package thor12022.diabolicalgems.gems;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.events.IGemArmorEventHandler;
import thor12022.diabolicalgems.potion.PotionRegistry;

class GarnetEventHandler extends AbstractGemSetEventHandler implements IGemArmorEventHandler
{

   GarnetEventHandler()
   {
      super(GemType.GARNET, PotionRegistry.potionBurning);
   }

   @Override
   public void onHit(DamageSource source, EntityLivingBase target, GemQuality quality, LivingHurtEvent event)
   {
      //! @todo probably won't work with Fire Aspect?
      if(!target.getEntityWorld().isRemote && 
         source.isFireDamage())
      {         
         //!@todo reduce damage rather than chance to stop it
         int bound = GemQuality.number() - quality.toMeta() - 1;
         if(bound == 0 ||
            DiabolicalGems.RAND.nextInt(bound) ==  0)
         {
            event.setCanceled(true);
         }
      }
   }
}
