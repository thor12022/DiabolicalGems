package thor12022.diabolicalgems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import thor12022.diabolicalgems.api.GemSetApiInternal;
import thor12022.diabolicalgems.api.exceptions.DiabolicalGemsException;
import thor12022.diabolicalgems.client.gui.CreativeTabBaseMod;
import thor12022.diabolicalgems.config.ConfigManager;
import thor12022.diabolicalgems.core.DiabolicalGemsApi;
import thor12022.diabolicalgems.proxies.CommonProxy;
import thor12022.diabolicalgems.util.TextHelper;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = ModInformation.ID, name = ModInformation.NAME, version = ModInformation.VERSION, dependencies = ModInformation.DEPEND)
public class DiabolicalGems
{
   @Mod.Instance
   public static DiabolicalGems instance;
   
   @SidedProxy(clientSide = ModInformation.CLIENTPROXY, serverSide = ModInformation.COMMONPROXY)
   public static CommonProxy proxy;
   
   public static final Logger             LOGGER         = LogManager.getLogger(ModInformation.NAME);
   public static final Random             RAND           = new Random();
   public static final ConfigManager      CONFIG         = new ConfigManager(ModInformation.ID);
   public static final CreativeTabs       CREATIVE_TAB   = new CreativeTabBaseMod(ModInformation.ID + ".creativeTab");
   public static final DiabolicalGemsApi  API            = new DiabolicalGemsApi();
   
   public DiabolicalGems()
   {
      try
      {
         GemSetApiInternal.setGemSetHandler(API);
      }
      catch(DiabolicalGemsException e)
      {
         LOGGER.error(e);
         LOGGER.fatal("Alight, here's the deal, I'm just minding my own business trying to Construct, but apprently I can't?!");
      }
   }
   
   @Mod.EventHandler
   public void preInit(@SuppressWarnings("unused") FMLPreInitializationEvent event)
   {
      LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.load.preInit"));

      proxy.preInit();
   }

   @Mod.EventHandler
   public void init(@SuppressWarnings("unused") FMLInitializationEvent event)
   {
      LOGGER.info(TextHelper.localize("info." + ModInformation.ID + ".console.load.init"));
      proxy.init();
   }
   
   @Mod.EventHandler
   public void postInit(@SuppressWarnings("unused") FMLPostInitializationEvent event)
   {
      proxy.postInit();
   }
}
