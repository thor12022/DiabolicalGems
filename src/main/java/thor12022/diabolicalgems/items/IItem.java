package thor12022.diabolicalgems.items;

public interface IItem
{
   public String name();
      
   public void registerItem();
   
   public void registerModel();
   
   public void registerRecipe();

   public boolean isEnabled();
}
