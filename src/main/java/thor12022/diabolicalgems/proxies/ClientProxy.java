package thor12022.diabolicalgems.proxies;

import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.blocks.BlockRegistry;

public class ClientProxy extends CommonProxy
{
   @Override
   public void preInit()
   {
      super.preInit();
      itemRegistry.registerModels();
      DiabolicalGems.API.registerModels();
      BlockRegistry.registerModels();
   }
   
   @Override
   public void init()
   {
      super.init();
      DiabolicalGems.API.registerModelColors();
   }
}
