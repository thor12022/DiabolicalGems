package thor12022.diabolicalgems.api;

import thor12022.diabolicalgems.api.exceptions.InvalidApiUsageException;

public class DiabolicalGems
{ 
   static IDiabolicalGemsApi instance;
   
   public static IDiabolicalGemsApi getApi() throws InvalidApiUsageException
   {
      if(instance != null)
      {
         return instance;
      }
      throw new InvalidApiUsageException("The Gem Set API has not been initialized yet");
   }
}
