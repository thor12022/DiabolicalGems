package thor12022.diabolicalgems.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.LoaderState;
import net.minecraftforge.fml.common.registry.GameRegistry;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.IGemSet;
import thor12022.diabolicalgems.api.events.IGemEventHandler;
import thor12022.diabolicalgems.api.exceptions.InvalidApiUsageException;
import thor12022.diabolicalgems.api.exceptions.InvalidGemSetException;
import thor12022.diabolicalgems.api.exceptions.UnknownGemSetException;
import thor12022.diabolicalgems.api.IDiabolicalGemsApi;
import thor12022.diabolicalgems.items.IItem;

public class DiabolicalGemsApi implements IDiabolicalGemsApi
{
   final Map<String, GemSet>   gemSets           =  new HashMap<>();
   final GemUseHandler         gemUseHandler     =  new GemUseHandler();
   final GemEventHandler       gemEventHandler   =  new GemEventHandler(this);
   
   private ItemStack creativeTabIcon;
      
   Enchantment socketEnchant = new EnchantmentSocket();
   
     
   public void register()
   {
      for(GemSet gemSet : gemSets.values())
      {
         ((IItem)gemSet.getItem()).registerItem();
         GameRegistry.register(gemSet.getEnchantment());
      }
      GameRegistry.register(socketEnchant);
   }
   
   public void postInit()
   {
      gemEventHandler.syncConfig();
   }
      
   public void registerModels()
   {
      for(GemSet gemSet : gemSets.values())
      {
         ((IItem)gemSet.getItem()).registerModel();
      }
   }

   public void registerModelColors()
   {
      for(GemSet gemSet : gemSets.values())
      {
         ((ItemGemSet)gemSet.getItem()).registerModelColors();
      }
   }
   
   @Override
   public Collection<? extends IGemSet> getGemSets()
   {
      return gemSets.values();
   }
   
   @Override
   public GemQuality getSocketedGemQuality(String gemName, ItemStack stack) throws UnknownGemSetException
   {
      try
      {
         int level = EnchantmentHelper.getEnchantmentLevel(gemSets.get(gemName).getEnchantment(), stack);
         return level > 0 ? GemQuality.fromMeta(level - 1) : null;
      }
      catch(@SuppressWarnings("unused") NullPointerException e)
      {
         throw new UnknownGemSetException(gemName);
      }
   }

   @Override
   public <EventHandlerClass extends IGemEventHandler> IGemSet createGemSet(String gemName, int color, EventHandlerClass eventHandler) throws InvalidApiUsageException, InvalidGemSetException
   {
      if(Loader.instance().isInState(LoaderState.CONSTRUCTING))
      {
         if(!gemSets.containsKey(gemName))
         {
            GemSet gemSet = new GemSet(gemName, color, gemEventHandler.registerEventHandler(gemName, eventHandler));
            gemSets.put(gemName, gemSet);
            if(creativeTabIcon == null)
            {
               creativeTabIcon = new ItemStack(gemSet.getItem(), 1, GemQuality.PERFECT.toMeta());
            }
            return gemSet;
         }
         return null;
      }
      throw new InvalidApiUsageException("Attempting to create Gem Set in wrong Loader state. Should be in CONSTRUCTING");
   }

   @Override
   public IGemSet getGemSet(String gemName)
   {
      return gemSets.get(gemName);
   }

   @Override
   public int getEmptySockets(ItemStack stack)
   {
      return EnchantmentHelper.getEnchantmentLevel(socketEnchant, stack);
   }

   @Override
   public GemQuality getCombinedArmorGemQuality(EntityLivingBase entity, String gemName) throws UnknownGemSetException
   {
      GemQuality bestQuality = null;
      for(ItemStack stack : entity.getArmorInventoryList())
      {
         try
         {
            int level = EnchantmentHelper.getEnchantmentLevel(gemSets.get(gemName).getEnchantment(), stack);
            if((bestQuality == null && level > 0) ||
               (bestQuality != null && bestQuality.isWorseThan(GemQuality.fromMeta(level - 1))))
            {
               bestQuality = GemQuality.fromMeta(level - 1);
            }
         }
         catch(@SuppressWarnings("unused") NullPointerException e)
         {
            throw new UnknownGemSetException(gemName);
         }         
      }
      return bestQuality;
   }

   public ItemStack getCreativeTabIcon()
   {
      if(creativeTabIcon != null)
      {
         return creativeTabIcon;
      }
      else
      {
         return new ItemStack(Items.EMERALD, 1);
      }
   }
}
