package thor12022.diabolicalgems.api;

import net.minecraftforge.fml.common.Loader;
import thor12022.diabolicalgems.api.exceptions.DiabolicalGemsException;

/**
 * The should be removed from the API Jar, and should not be used
 *
 */
public class GemSetApiInternal
{
   public static void setGemSetHandler(IDiabolicalGemsApi gemSetApi) throws DiabolicalGemsException
   {
      if(Loader.instance().activeModContainer().getMod() == DiabolicalGems.instance)
      {
         DiabolicalGems.instance = gemSetApi;
      }
      else
      {
         throw new DiabolicalGemsException("Improper API Initialization");
      }
   }
}
