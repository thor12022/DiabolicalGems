package thor12022.diabolicalgems.potion;

import net.minecraft.client.Minecraft;
import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import thor12022.diabolicalgems.ModInformation;
import thor12022.diabolicalgems.config.Config;

public abstract class AbstractPotion extends Potion
{
   private static final ResourceLocation RESOURCE = new ResourceLocation(ModInformation.ID + ":/textures/guis/icons.png");

   @Config(minInt=1)
   int baseInterval = 20;

   protected AbstractPotion(String name, boolean isBadEffectIn, int liquidColorIn, int index)
   {
      super(isBadEffectIn, liquidColorIn);
      this.setIconIndex( index % 8, index / 8);
      this.setPotionName("potion." + ModInformation.ID + "." + name);
      setRegistryName(name);
   }

   @Override
   public boolean isReady(int duration, int level)
   {
      int i = baseInterval >> level;
      return i > 0 ? duration % i == 0 : true;
   }
   
   @Override
   @SideOnly(Side.CLIENT)
   public int getStatusIconIndex()
   {
      Minecraft.getMinecraft().renderEngine.bindTexture(RESOURCE);
      return super.getStatusIconIndex();
   }

}
