package thor12022.diabolicalgems.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.WeightedRandom;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.AnvilUpdateEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import thor12022.diabolicalgems.DiabolicalGems;
import thor12022.diabolicalgems.api.GemQuality;
import thor12022.diabolicalgems.api.IGemSet;
import thor12022.diabolicalgems.api.events.IGemArmorEventHandler;
import thor12022.diabolicalgems.api.events.IGemEventHandler;
import thor12022.diabolicalgems.api.events.IGemShieldEventHandler;
import thor12022.diabolicalgems.api.events.IGemToolEventHandler;
import thor12022.diabolicalgems.api.events.IGemWeaponEventHandler;
import thor12022.diabolicalgems.api.exceptions.DiabolicalGemsException;
import thor12022.diabolicalgems.api.exceptions.InvalidGemSetException;
import thor12022.diabolicalgems.api.exceptions.UnknownGemSetException;
import thor12022.diabolicalgems.config.Config;
import thor12022.diabolicalgems.config.Configurable;

@Configurable(sectionName=Configurable.SECTION_GENERAL, syncNotification="syncConfig")
public class GemEventHandler
{   
   private static class WeightedGemQuality extends WeightedRandom.Item
   {
      final GemQuality quality;
      
      public WeightedGemQuality(GemQuality quality, int itemWeightIn)
      {
         super(itemWeightIn);
         this.quality = quality;
      }  
   }
   
   private ArrayList<Class<? extends EntityLivingBase>> dropEntities       = new ArrayList<>();
   private ArrayList<Class<? extends EntityLivingBase>> dropBaseEntities   = new ArrayList<>();
   private ArrayList<WeightedGemQuality> weightedGemQualities              = new ArrayList<>();
   

   private Map<String, IGemArmorEventHandler>   armorEventHandlers   =  new HashMap<>();
   private Map<String, IGemShieldEventHandler>  shieldEventHandlers  =  new HashMap<>();
   private Map<String, IGemToolEventHandler>    toolEventHandlers    =  new HashMap<>();
   private Map<String, IGemWeaponEventHandler>  weaponEventHandlers  =  new HashMap<>();
   
   final private DiabolicalGemsApi api;

   @Config(minFloat=0f)
   private float gemCombiningXpMultiplier = 6;

   @Config(minFloat=0f)
   private float gemSocketingXpMultiplier = 6;   

   @Config(minFloat=1f, maxFloat=10f, comment="w=gemQualityRarityCoeff * q^gemQualityRarityExp")
   private float gemQualityRarityExp = 3;
   
   @Config(minFloat=1f, comment="w=gemQualityRarityCoeff * q^gemQualityRarityExp")
   private float gemQualityRarityCoeff = 4;
   
   @Config(minInt=0, comment="1 out of gemDropRarity")
   private int gemDropRarity = 250;
   
   @Config(minInt=1)
   private int maxBossDropStackSize = 4;

   @Config
   private boolean dropGems = true;
   
   @Config(comment="Types of base entities that drop gems. For example, 'EntityZombie' will work for Zombies and Zombie Pigmen, 'EntitySkeleton' will also work for Wither Skeletons")
   String[] dropBaseEntitiesNames = 
      {
         "Witch",
         "Enderman",
         "Blaze",
         "WitherBoss",
         "Guardian",
         "Silverfish"
      }; 
   
   @Config(comment="Specific entities that drop gems")
   String[] dropEntitiesNames = 
      {
         "PigZombie",
         "EnderDragon"
      }; 
   
   GemEventHandler(DiabolicalGemsApi api)
   {
      this.api = api;
      DiabolicalGems.CONFIG.register(this);
      for(GemQuality quality : GemQuality.values())
      {
         weightedGemQualities.add(new WeightedGemQuality(quality, (int)(gemQualityRarityCoeff * Math.pow((GemQuality.number() - quality.toMeta()), gemQualityRarityExp))));
      }   
      MinecraftForge.EVENT_BUS.register(this);
   }
   
   <EventHandlerClass extends IGemEventHandler> ArrayList<GemUseTypes> registerEventHandler(String gemType, EventHandlerClass eventHandler) throws InvalidGemSetException
   {
      ArrayList<GemUseTypes> validUses = new ArrayList<>();
      if(eventHandler instanceof IGemArmorEventHandler)
      {
         validUses.add(GemUseTypes.ARMOR);
         armorEventHandlers.put(gemType, (IGemArmorEventHandler)eventHandler);
      }
      if(eventHandler instanceof IGemShieldEventHandler)
      {
         validUses.add(GemUseTypes.SHIELD);
         shieldEventHandlers.put(gemType, (IGemShieldEventHandler)eventHandler);
      }
      if(eventHandler instanceof IGemToolEventHandler)
      {
         validUses.add(GemUseTypes.TOOL);
         toolEventHandlers.put(gemType, (IGemToolEventHandler)eventHandler);
      }
      if(eventHandler instanceof IGemWeaponEventHandler)
      {
         validUses.add(GemUseTypes.WEAPON);
         weaponEventHandlers.put(gemType, (IGemWeaponEventHandler)eventHandler);
      }
      if(validUses.size() == 0)
      {
         throw new InvalidGemSetException(gemType, "No valid uses");
      }
      return validUses;
   }
   
   /**
    * Called by the ConfigManager when the CONFIG is updated<BR>
    * @post dropBaseEntities & dropEntites are populated by CONFIG values
    */
   public void syncConfig()
   {
      dropBaseEntities.clear();
      for(String entityName : dropBaseEntitiesNames)
      {
         Class<? extends Entity> entityClass = EntityList.NAME_TO_CLASS.get(entityName);
         try
         {
            dropBaseEntities.add(entityClass.asSubclass(EntityLiving.class));
         }
         catch(ClassCastException | NullPointerException exp)
         {
            DiabolicalGems.LOGGER.warn(exp);
            DiabolicalGems.LOGGER.warn("Cannot add ", entityName, " not a valid entity");
         }
      }
      
      dropEntities.clear();
      for(String entityName : dropEntitiesNames)
      {
         Class<? extends Entity> entityClass = EntityList.NAME_TO_CLASS.get(entityName);
         try
         {
            dropEntities.add(entityClass.asSubclass(EntityLiving.class));
         }
         catch(ClassCastException | NullPointerException exp)
         {
            DiabolicalGems.LOGGER.warn(exp);
            DiabolicalGems.LOGGER.warn("Cannot add ", entityName, " not a valid entity");
         }
      }
   }
 
   private boolean isEntityGeneric(EntityLivingBase entity)
   {
      for(Class<? extends EntityLivingBase> genericEntity : dropBaseEntities)
      {
         if(genericEntity.isInstance(entity))
         {
            return true;
         }
      }
      return false;
   }
   
   @SubscribeEvent
   public void anvilUpdateEvent(AnvilUpdateEvent event)
   {
      
      if(event.getRight().getItem() instanceof ItemGemSet)
      {
         ItemGemSet gemSet = (ItemGemSet)event.getRight().getItem();
         // They need to be the same type & quality to combine
         if(event.getLeft().getItem() == event.getRight().getItem())
         {
            if(ItemGemSet.getGemQuality(event.getRight()) == ItemGemSet.getGemQuality(event.getLeft()))
            {
               // they need to be less than perfect
               if(ItemGemSet.getGemQuality(event.getRight()).isWorseThan(GemQuality.PERFECT))
               {
                  // create a gem of the next better quality
                  ItemStack result = new ItemStack(gemSet, 1, ItemGemSet.getGemQuality(event.getRight()).getNextBetter().toMeta());
                  event.setCost((int)((event.getRight().getMetadata() + 1) * gemCombiningXpMultiplier));
                  event.setMaterialCost(1);
                  event.setOutput(result);
               }
            }
         }
         else if(api.gemUseHandler.canUseGemOnItem(event.getLeft(), api.gemSets.get(gemSet.getGemType())))
         {
            if(EnchantmentHelper.getEnchantmentLevel(api.socketEnchant, event.getLeft()) > 0)
            {
               ItemStack result = ItemStack.copyItemStack(event.getLeft());
               Enchantment gemEnchant = ((GemSet)api.getGemSet(gemSet.getGemType())).getEnchantment();
               Map<Enchantment, Integer> enchantMap = EnchantmentHelper.getEnchantments(result);
               if(!enchantMap.containsKey(gemEnchant))
               {
                  int sockets = enchantMap.get(api.socketEnchant);
                  if(sockets > 0)
                  {
                     enchantMap.remove(api.socketEnchant);
                     if( sockets > 1)
                     {
                        enchantMap.put(api.socketEnchant, sockets - 1);
                     }
                     enchantMap.put(gemEnchant, Integer.valueOf(ItemGemSet.getGemQuality(event.getRight()).toMeta() + 1));
                     EnchantmentHelper.setEnchantments(enchantMap, result);
                     event.setCost((int)((event.getRight().getMetadata() + 1) * gemSocketingXpMultiplier));
                     event.setMaterialCost(1);
                     event.setOutput(result);
                  }
               }
            }
         }
      }
   }
   
   /**
    * 
    * @todo Replace with Loot Tables?
    */
   @SubscribeEvent
   public void livingDropsEvent(LivingDropsEvent event)
   {
      if(dropGems && (gemDropRarity == 0 || DiabolicalGems.RAND.nextInt(gemDropRarity) == 1))
      {
         if(dropEntities.contains(event.getEntityLiving()) || isEntityGeneric(event.getEntityLiving()))
         {
            WeightedGemQuality weightedQuality = WeightedRandom.getRandomItem(DiabolicalGems.RAND, weightedGemQualities);
            int stackSize = event.getEntityLiving().isNonBoss() ? 1 : (int)(Math.abs(DiabolicalGems.RAND.nextGaussian()) * maxBossDropStackSize);
            //! TODO Cleanse the stupid that is the next line
            String gemName = ((IGemSet)api.gemSets.keySet().toArray()[DiabolicalGems.RAND.nextInt(api.gemSets.keySet().size())]).getName();
            ItemStack dropStack = new ItemStack( api.gemSets.get(gemName).getItem(), stackSize, weightedQuality.quality.toMeta());
            event.getDrops().add(new EntityItem(event.getEntityLiving().getEntityWorld(), 
                                                event.getEntityLiving().posX, 
                                                event.getEntityLiving().posY, 
                                                event.getEntityLiving().posZ, 
                                                dropStack));
         }
      }
   }
   
   @SubscribeEvent
   public void onHurt(LivingHurtEvent event)
   {
      if(event.getEntity() instanceof EntityLivingBase)
      {
         if(event.getSource().getEntity() instanceof EntityLivingBase)
         {
            // First handle the Gemmed weapons
            ItemStack hittingItemStack =  ((EntityLivingBase)event.getSource().getEntity()).getHeldItemMainhand();
            if(hittingItemStack != null)
            {
               for(String gemType : weaponEventHandlers.keySet())
               {
                  try
                  {
                     GemQuality quality = api.getSocketedGemQuality(gemType, hittingItemStack);
                     if(quality != null)
                     {
                        weaponEventHandlers.get(gemType).onAttack((EntityLivingBase)event.getSource().getEntity(), (EntityLivingBase)event.getEntity(), quality, event);
                        if(event.isCanceled())
                        {
                           // The Gem Event canceled it, get out of here
                           break;
                        }
                     }
                  }
                  catch(DiabolicalGemsException e)
                  {
                     DiabolicalGems.LOGGER.error(e);
                     DiabolicalGems.LOGGER.error("Error Processing Gem Weapon event for " + gemType + " disabling.");
                     weaponEventHandlers.remove(gemType);
                     break; // avoid ConcurentModificationException and using having to use Iterators
                  }
               }
            }
         }
         // Deal with the Armor events
         for(String gemType : weaponEventHandlers.keySet())
         {
            try
            {
               GemQuality quality = api.getCombinedArmorGemQuality((EntityLivingBase)event.getEntity(), gemType);
               if(quality != null)
               {
                  armorEventHandlers.get(gemType).onHit(event.getSource(), (EntityLivingBase)event.getEntity(), quality, event);
                  if(event.isCanceled())
                  {
                     // The Gem Event canceled it, get out of here
                     break;
                  }
               }
            }
            catch(UnknownGemSetException e)
            {
               DiabolicalGems.LOGGER.error(e);
               DiabolicalGems.LOGGER.error("Error Processing Gem Weapon event for " + gemType + " disabling.");
               weaponEventHandlers.remove(gemType);
               break; // avoid ConcurentModificationException and using having to use Iterators
            }
         }
      }
   }
}
